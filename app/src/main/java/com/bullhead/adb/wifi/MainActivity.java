package com.bullhead.adb.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.widget.Button;
import android.widget.TextView;
import com.bullhead.adb.wifi.libsuperuser.TaskRunner;

public class MainActivity extends AppCompatActivity implements TaskRunner.OnAvailableListener {

    private Button btnStart;
    private TextView tvStatus;

    private TaskRunner taskRunner;

    private String ip;
    private WifiManager wifiManager;
    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ip = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
            onAvailable();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        taskRunner = TaskRunner.getInstance();
        btnStart = findViewById(R.id.btnStart);
        tvStatus = findViewById(R.id.tvStatus);
        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

    }

    @Override
    public void onAvailable() {
        if (ip == null || ip.equals("0.0.0.0")) {
            btnStart.setEnabled(false);
            tvStatus.setText(R.string.not_connected_to_network);
        } else {
            checkIfStarted();
        }
    }

    private void checkIfStarted() {
        taskRunner.run((result) -> {
            if (result != null && result.size() > 0 && result.get(0).contains("5555")) {
                btnStart.setOnClickListener((v) -> stopNetwork());
                tvStatus.setText(getString(R.string.running_at, ip));
                btnStart.setText(R.string.stop_adb_network);

            } else {
                btnStart.setText(getString(R.string.start_adb_network));
                btnStart.setEnabled(true);
                tvStatus.setText(R.string.not_running);
                btnStart.setOnClickListener((v) -> startNetwork());
            }
        }, "getprop | grep service.adb.tcp.port");
    }

    @Override
    public void onUnavailable() {
        btnStart.setEnabled(false);
        tvStatus.setText(R.string.root_required);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
            registerReceiver(networkReceiver, filter);
        } catch (Exception e) {
            //ignore
        }
        taskRunner.checkAvailability(this);
    }

    private void startNetwork() {
        String[] commands = new String[3];
        commands[0] = "setprop service.adb.tcp.port 5555";
        commands[1] = "stop adbd";
        commands[2] = "start adbd";
        taskRunner.run((result) -> {
            checkIfStarted();
        }, commands);
    }

    private void stopNetwork() {
        String[] commands = new String[3];
        commands[0] = "setprop service.adb.tcp.port -1";
        commands[1] = "stop adbd";
        commands[2] = "start adbd";
        taskRunner.run((result) -> {
            checkIfStarted();
        }, commands);
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(networkReceiver);
        } catch (Exception e) {
            //ignore
        }
        super.onPause();
    }
}
